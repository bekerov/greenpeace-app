<?php

class UserProfileController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        // Текущий пользователь
        $current_user = Auth::user();

        //Получаем из таблицы анкет первую с id пользователя
        $form = EntryForms::where('user_id', '=', $current_user->id)->first();
        if ($form) {
            return $form;
        } else {
            EntryForms::create([
                'user_id'=> $current_user->id
            ]);
            $form = EntryForms::where('user_id', '=', $current_user->id)->first();
            return $form;
        }

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
        // Текущий пользователь
        $current_user = Auth::user();

        //Получаем из таблицы анкет первую с id пользователя
        $data = EntryForms::where('user_id', '=', $current_user->id)->first();

        //Пришедшие данные
        $input = Input::only(
            'firstname',
            'lastname',
            'middlename',
            'contact_phone',
            'email',
            'mobile_phone',
            'org',
            'org_addres',
            'user_id',
            'is_blocked',
            'department',
            'job',
            'experience',
            'gis_samples',
            'official_invitation',
            'invite_fio',
            'invite_appeal',
            'invite_address',
            'independent_booking',
            'i_can_settle',
            'i_need_flat',
            'oral_report',
            'oral_report_projector',
            'oral_report_internet',
            'oral_report_flash',
            'oral_report_audio',
            'oral_report_markers',
            'oral_report_other',
            'oral_report_name',
            'oral_report_annotation',
            'stand_report',
            'stand_report_a0_count',
            'stand_report_a1_count',
            'stand_report_a2_count',
            'roundtable',
            'roundtable_theme',
            'listener',
            'volunteer_assistance',
            'volunteer_assistance_help'
        );

        $data->fill($input);
        $data->save();

        return Response::json(array(
            'error' => false,
            'data' => $data,
            200
        ));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
