<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 22.01.15
 * Time: 19:11
 */

class EntryForms extends Eloquent {

    protected $table = 'entry_forms';

    protected $fillable = [
        'firstname',
        'lastname',
        'middlename',
        'contact_phone',
        'email',
        'mobile_phone',
        'org',
        'org_addres',
        'user_id',
        'is_blocked',
        'department',
        'job',
        'experience',
        'gis_samples',
        'official_invitation',
        'invite_fio',
        'invite_appeal',
        'invite_address',
        'independent_booking',
        'i_can_settle',
        'i_need_flat',
        'oral_report',
        'oral_report_projector',
        'oral_report_internet',
        'oral_report_flash',
        'oral_report_audio',
        'oral_report_markers',
        'oral_report_other',
        'oral_report_name',
        'oral_report_annotation',
        'stand_report',
        'stand_report_a0_count',
        'stand_report_a1_count',
        'stand_report_a2_count',
        'roundtable',
        'roundtable_theme',
        'listener',
        'volunteer_assistance',
        'volunteer_assistance_help'
    ];

    public function user() {
        return $this->belongsTo('User', 'user_id');
    }

}