<?php
/**
 * Created by PhpStorm.
 * User: Bekerov Artur http://bekerov.ru
 * Date: 21.01.15
 * Time: 23:11
 */

class Role extends Eloquent {

    protected $fillable = ['name'];

}