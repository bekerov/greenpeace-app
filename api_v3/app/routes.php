<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {
    return "Api v3 <br/> artur@bekerov.ru";
});


Route::get('/demo', function() {
//clear cache
//    Cache::flush();



    // Создание пользователя
//    $data = User::create([
//        'email'=>'c@c',
//        'password'=>Hash::make('1234')
//    ]);
//    User::find($data->id)->assignRole(2);

    //авторизован ли
//    if (Auth::user()->hasRole('user')) {
//        return 'have';
//    } else {
//        return 'no';
//
//    }

    // удаление пользователя
//    $model = User::first();
//    $model->delete();
//    return $model;



//    Присвоение роли
//    User::first()->assignRole(1);
//    return "ok";
//    return User::find(5);

//    Возвращение кода
//    return Response::json(array(), 400);

});


//Route::get('admin', function() {
//    return 'admin page';
//})->before('role:admin');


//
//Route::put('profile', function() {
//    return 'ok';
//});



//Авторизация;
Route::post('login', 'SessionsController@store');
Route::get('logout', 'SessionsController@destroy');

//Сессии
Route::resource('sessions', "SessionsController");

//Регистрация
Route::post('register', 'UserController@create');

//Личный кабинет
Route::get('profile', 'UserProfileController@index')->before('role:user');
Route::put('profile', 'UserProfileController@update')->before('role:user');



//
//Админские роуты
//
//Пользователи
Route::get('users', 'UserController@index')->before('role:user');

//Заполненные анкеты
Route::get('entry_forms', 'EntryFormsController@index')->before('role:user');
Route::get('entry_forms/{id}', 'EntryFormsController@show')->before('role:user');
Route::put('entry_forms/{id}', 'EntryFormsController@update')->before('role:user');

