-- --------------------------------------------------------
-- Хост:                         vds.bekerov.ru
-- Версия сервера:               5.5.40-0ubuntu0.14.04.1 - (Ubuntu)
-- ОС Сервера:                   debian-linux-gnu
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица bekerov_greenpe.entry_forms
CREATE TABLE IF NOT EXISTS `entry_forms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `middlename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `org` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `org_addres` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone_var` tinyint(1) DEFAULT NULL,
  `job` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `experience` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gis_samples` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `i_need_flat` tinyint(1) DEFAULT NULL COMMENT 'Мне нужнj жилье в Москве',
  `i_can_settle` tinyint(1) DEFAULT NULL COMMENT 'Я могу поселить',
  `independent_booking` tinyint(1) DEFAULT NULL COMMENT 'Самостоятельное бронирования',
  `official_invitation` tinyint(1) DEFAULT NULL,
  `invite_fio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invite_appeal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invite_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oral_report` int(11) DEFAULT NULL,
  `oral_report_projector` tinyint(1) DEFAULT NULL,
  `oral_report_internet` tinyint(1) DEFAULT NULL,
  `oral_report_flash` tinyint(1) DEFAULT NULL,
  `oral_report_audio` tinyint(1) DEFAULT NULL,
  `oral_report_markers` tinyint(1) DEFAULT NULL,
  `oral_report_other` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oral_report_lang` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oral_report_name` text COLLATE utf8_unicode_ci,
  `oral_report_annotation` text COLLATE utf8_unicode_ci,
  `stand_report` tinyint(1) DEFAULT NULL,
  `stand_report_a0_count` int(11) DEFAULT NULL,
  `stand_report_a1_count` int(11) DEFAULT NULL,
  `stand_report_a2_count` int(11) DEFAULT NULL,
  `roundtable` tinyint(1) DEFAULT NULL,
  `roundtable_theme` text COLLATE utf8_unicode_ci,
  `listener` tinyint(1) DEFAULT NULL,
  `volunteer_assistance` tinyint(1) DEFAULT NULL,
  `volunteer_assistance_help` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_blocked` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы bekerov_greenpe.entry_forms: ~14 rows (приблизительно)
/*!40000 ALTER TABLE `entry_forms` DISABLE KEYS */;
INSERT INTO `entry_forms` (`id`, `user_id`, `firstname`, `middlename`, `lastname`, `org`, `org_addres`, `email`, `mobile_phone`, `contact_phone`, `contact_phone_var`, `job`, `experience`, `gis_samples`, `department`, `i_need_flat`, `i_can_settle`, `independent_booking`, `official_invitation`, `invite_fio`, `invite_appeal`, `invite_address`, `oral_report`, `oral_report_projector`, `oral_report_internet`, `oral_report_flash`, `oral_report_audio`, `oral_report_markers`, `oral_report_other`, `oral_report_lang`, `oral_report_name`, `oral_report_annotation`, `stand_report`, `stand_report_a0_count`, `stand_report_a1_count`, `stand_report_a2_count`, `roundtable`, `roundtable_theme`, `listener`, `volunteer_assistance`, `volunteer_assistance_help`, `is_blocked`, `created_at`, `updated_at`) VALUES
	(4, '35', 'Артур', 'Александрович', 'Бекеров1', 'BKR', 'Россия, Томск', 'artur@bekerov.ru', '9539200365', '9539200365', NULL, 'Генеральный директор', 'Небольшой опыт (1-2 года)', 'Да я везде использую ГИС)))', 'Предприниматель', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, 1, 'Принтер', 'ru_en', 'Мониторинг', 'мониторинг', 0, 2, 1, 2, 0, 'Тема', NULL, NULL, NULL, 'false', '2015-01-22 19:58:49', '2015-02-27 08:22:57'),
	(11, '47', 'Анна', 'Федоровна', 'Комарова', 'сохраняется', 'сохраняется', 'akomarov@greenpeace.org', '9030000000', '9030000000', NULL, 'сохраняется', 'Значительный опыт (более 2 лет)', 'пользуюсь яндекс-пробками', 'Сотрудник НКО', 1, NULL, 1, 0, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 1, NULL, NULL, 'Хочу летать', 'Хочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летатьХочу летать', NULL, NULL, NULL, NULL, 1, 'Хочу летатьХочу летатьХочу летатьХочу летать очень', NULL, 1, NULL, 'false', '2015-01-23 10:46:22', '2015-03-01 14:37:25'),
	(12, '48', 'Илона', 'Вячеславовна', 'Журавлева', 'Greenpeace Гринпис', 'Россия, Москва, Ленинградский проспект 26, 1', 'ilona.zhuravleva@greenpeace.org', '9164269357', '9065537654', NULL, 'руководитель ГИС отдела', 'Значительный опыт (более 2 лет)', 'тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест тесттесттесттесттесттесттесттесттесттест', 'Сотрудник НКО', 0, 1, NULL, 1, 'Руководителю ООО "Тест" Иванову Ивану Ивановичу', 'Уважаемый Иван Иванович', '124589 Московская область, Люберцы, ул. Ленина 1, 1', 1, 1, NULL, 1, NULL, 1, 'тест', NULL, 'тест тест тест', 'тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест', 1, 1, NULL, NULL, 1, 'я хочу провести круглый стол.....', 1, 1, 'помощь в регистрации, еда', 'false', '2015-01-23 11:06:03', '2015-03-16 11:26:37'),
	(13, '23', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'true', '2015-01-27 11:23:56', '2015-01-27 19:18:54'),
	(14, '50', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', '2015-01-28 09:44:02', '2015-01-30 14:31:06'),
	(15, '51', 'Имя', 'Отчество', 'Фамилия', 'Организация', 'Адрес организации', '15@test.ru', '9539200365', '9539200365', NULL, 'Должность', 'Небольшой опыт (1-2 года)', 'Опишите примеры использования ГИС-технологий для решения задач охраны природы в Вашей работе. *', 'Представитель органов государственной власти', 1, 1, 1, 1, 'Губернатору Ульяновской области С.И.Морозову', 'Уважаемый Сергей Иванович', '432017 Россия, г. Ульяновск, пл. Ленина, д.1', 1, 1, 1, 1, 1, 1, NULL, NULL, '123213', '123213', 1, 1, 1, 2, 1, '123123123', 1, 1, NULL, 'false', '2015-02-27 08:28:48', '2015-03-01 19:12:51'),
	(16, '52', 'Пам', 'Пам', 'Трам', 'трям', 'трям', 'cvbnm@yandex.ru', '0000000000', NULL, NULL, 'трям', 'Небольшой опыт (1-2 года)', 'уже', 'Сотрудник НИИ', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Влияние ГИС на творчество крыс', 'Влияние ГИС на творчество крыс - проблема важнейшая. Где компромисс?', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', '2015-03-01 14:27:03', '2015-03-01 14:29:50'),
	(17, '53', '2', '3', '1', '123', '123', '123@123.ru', '1231231231', '1232123211', NULL, '321', 'Небольшой опыт (1-2 года)', 'раз попытка, два попытка, три - провалена', '33333333', NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 3, 1, 'можно ли замкнуть в кольцо натуральные числа от одного до трех?', NULL, 1, NULL, 'false', '2015-03-01 14:30:51', '2015-03-01 14:37:43'),
	(18, '54', 'гис', 'гис', 'Гис', '', '', 'designing@future.com', '6234275476', '6234275476', NULL, NULL, 'Только планируете использовать ГИС', 'designing our future', 'нет меня', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 'designing our future', 'designing our future и выбираем лучший', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', '2015-03-01 14:40:16', '2015-03-01 14:46:02'),
	(19, '55', 'Дарья', '', 'Зудкина', 'Гринпис', '', 'dashazu@kina.com', '6537489764', NULL, NULL, 'стажер', 'Небольшой опыт (1-2 года)', 'Помогала папе цифровать плантации; планирую поделиться опытом с зарубежными коллегами. Больше всего мне нравится мышка.', 'стажер', NULL, 1, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, 'а также еще что-нибудь погрызть', NULL, 'Опыт привлечения к работе детей в возрасте до 3 месяцев', 'Это кошмар', NULL, NULL, NULL, NULL, 1, 'Можно ли впитать ГИС с молоком матери и как вводить прикорм в этом случае. Мастер-класс по впитыванию молока', NULL, 1, NULL, 'false', '2015-03-01 14:48:15', '2015-03-01 14:56:41'),
	(20, '56', 'фы', 'фы', 'фы', 'фырганизация', 'фыдрес', 'as@as.ru', '9999999999', NULL, NULL, '', 'Значительный опыт (более 2 лет)', 'ф самом деле важныя фышка', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', '2015-03-01 16:37:34', '2015-03-01 16:39:53'),
	(21, '57', 'мыла', 'раму', 'Мама', '', '', 'mylo@gmylo.com', '6786786787', NULL, NULL, NULL, NULL, 'C горем пополам', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', '2015-03-01 16:41:05', '2015-03-01 16:43:09'),
	(22, '58', 'семенович', 'семенов', '77', 'общество трудоголиков', '', '7@77.ru', '7777777777', NULL, NULL, NULL, NULL, '777', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'мвмфоым', 'ворсосвосф', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', '2015-03-01 16:49:22', '2015-03-01 16:51:37'),
	(23, '59', 'p', '', 'parol', '', '', 'parol@mylo.ru', '2456497608', NULL, NULL, NULL, NULL, 'herf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'false', '2015-03-01 16:52:41', '2015-03-01 16:53:28');
/*!40000 ALTER TABLE `entry_forms` ENABLE KEYS */;


-- Дамп структуры для таблица bekerov_greenpe.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы bekerov_greenpe.migrations: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES
	('2015_01_22_050228_create_users_table', 1),
	('2015_01_22_050644_create_roles_table', 1),
	('2015_01_22_060705_create_role_user_table', 1),
	('2015_01_22_123041_create_entry_forms_table', 2),
	('2015_01_22_124425_create_entry_forms_table_add_id', 3),
	('2015_01_22_124724_create_entry_forms_table_add_id2', 4),
	('2015_01_22_181957_edit_users_table_rememberToken', 5),
	('2015_01_27_084805_create_soft_del_for_users', 6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Дамп структуры для таблица bekerov_greenpe.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы bekerov_greenpe.roles: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(2, 'user', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Дамп структуры для таблица bekerov_greenpe.role_user
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы bekerov_greenpe.role_user: ~31 rows (приблизительно)
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(2, 2, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 2, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 2, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(6, 2, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 2, 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 2, 14, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 2, 15, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 2, 18, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 2, 19, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 2, 23, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 2, 35, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(14, 2, 36, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(15, 2, 37, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(16, 2, 41, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(17, 2, 43, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(18, 2, 44, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(19, 2, 45, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(20, 2, 46, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(21, 2, 47, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(22, 2, 48, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(23, 2, 50, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(24, 2, 51, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(25, 2, 52, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(26, 2, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(27, 2, 54, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(28, 2, 55, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(29, 2, 56, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(30, 2, 57, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(31, 2, 58, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(32, 2, 59, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;


-- Дамп структуры для таблица bekerov_greenpe.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы bekerov_greenpe.users: ~34 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(5, 'bekerov.artur@yandex.ru', '$2y$10$Y73Y3AR19miI5OJnF8jj..ZenYVqFm6onOl3IjrcS1VA6HQ6CUbri', NULL, '2015-01-22 09:39:15', '2015-01-27 08:53:13', NULL),
	(7, 'test@test.ru', '$2y$10$bAlnJ.wvD3kCb..nbFX1cOMi1jvcSVMW2/2GH./Da5iTjwwy9.hqe', NULL, '2015-01-22 09:44:01', '2015-01-22 09:44:01', NULL),
	(8, 'test2@bekerov.ru', '$2y$10$QzG2Xo0whOIKUlQUAEmt.uP0Pr.GVEdXe6lE44wAr2Lm2bgRuQD3e', NULL, '2015-01-22 09:48:06', '2015-01-22 09:48:06', NULL),
	(9, 'artur2@bekerov.ru', '$2y$10$PQmdpT3Z124tbSFeQQQyAuqNluUUEekPTz3azdtjlLwqwYslyKmOa', NULL, '2015-01-22 09:48:38', '2015-01-22 09:48:38', NULL),
	(10, 'artur3@bekerov.ru', '$2y$10$6RJ/mkyUJVI4AF2Ay03GkeQrUCyVq5fZIcAsidhIDVOwyZ3BsIiam', NULL, '2015-01-22 09:49:27', '2015-01-22 09:49:27', NULL),
	(12, 'test1@test.ru', '$2y$10$R1pLF3S7hLFUxQvPf.9tZOlfeJhoRNwdueDDi7SWzJUF3CpXTQ202', NULL, '2015-01-22 18:25:58', '2015-01-22 18:25:58', NULL),
	(13, 'test2@test.ru', '$2y$10$Zi89QkyCFSpC/AqI9LXM2ugN/6Wo16CmUfBywp3HQuiDl5IgW5fnq', NULL, '2015-01-22 18:27:21', '2015-01-22 18:27:21', NULL),
	(14, 'test5@test.ru', '$2y$10$q0WvIQNei3DmJRYvrWArRucA2Hr10LF5dNrZigspGSMZLxDOTV9Ce', NULL, '2015-01-22 18:51:48', '2015-01-22 18:51:48', NULL),
	(15, 'test6@test.ru', '$2y$10$kOBmaK0x22DjZDPSMg/PUOyGOHJIODrs4RNiR5s7YxxhI9eGTYz9q', NULL, '2015-01-22 18:52:14', '2015-01-22 18:52:14', NULL),
	(18, 'test7@test.ru', '$2y$10$7.ijK.wggJSWCZI8DyFK2eTSHATkWTjmI90oRcazIMO.MCeTQHb1S', NULL, '2015-01-22 18:53:11', '2015-01-22 18:53:11', NULL),
	(19, 'test8@test.ru', '$2y$10$yZstuurtStS4.4oHkgby8.luQnb87sB9UHVg43whj5BVz7lazUnxe', NULL, '2015-01-22 18:54:11', '2015-01-22 18:54:11', NULL),
	(23, 'test9@test.ru', '$2y$10$ydP0ylWuYcvE35Qh9qYYz.s2xLsk/41eDTqH3sMlrT9lV37HOU.Z2', NULL, '2015-01-22 18:56:00', '2015-01-22 18:56:00', NULL),
	(35, 'test10@test.ru', '$2y$10$4BaM/UI1ppjSNyQ6PXzNq.VY3zUrUK5WaIJ6bjrXMmO.Nhyfpxt4C', 'LuvWJS3cSLnUL0PAhwn7A7bmlMhCrurdzmOOWh4Xufmdd4cGZxMD0Lt1vTcS', '2015-01-22 18:57:03', '2015-03-01 08:47:24', NULL),
	(36, 'artur@krekerov.ru', '$2y$10$BFm/ue15.cw5WFQuCOIuQOinVJ/fK3u1jrNCgNYTmDuqbOtTW6gTW', 'jWi5zF9Zu1sQSLJH0muV0h2xbsD2BGiLCGHUNXUJ4XPV456anLqMFP9RMrMu', '2015-01-22 19:07:17', '2015-01-22 19:07:17', NULL),
	(37, 'a@b', '$2y$10$ayQUUBiJ58Alzzfgb3oI0O5WDmaFVLDipd2DmieRFo54lNHKW6.vu', 'jWi5zF9Zu1sQSLJH0muV0h2xbsD2BGiLCGHUNXUJ4XPV456anLqMFP9RMrMu', '2015-01-22 19:23:24', '2015-01-22 19:23:24', NULL),
	(38, 'b@b', '$2y$10$ASDHnbSnzeYptcaRgVsR.OmgpCJu9E56i0qfNn7sgc9CsWYQa9Be.', NULL, '2015-01-22 19:27:41', '2015-01-22 19:27:41', NULL),
	(39, 'a@a', '$2y$10$vj87WTSo47mLefBECAGCTuQVOO799sYBuFvTHRotuOqLcKSA3Nu36', NULL, '2015-01-22 19:44:36', '2015-01-22 19:44:36', NULL),
	(41, 'c@c', '$2y$10$oSEsUVK5SMPrrdDKMUFuje4bR7ide2B5PhIgaynj38QKtbJVlQpUK', NULL, '2015-01-22 19:49:29', '2015-01-22 19:49:29', NULL),
	(43, 'test11@test.ru', '$2y$10$zFoBL.SadvSr5owSfLPRde97.0KoaDKdAz9XrtudogVVuEBMi0KiW', 'nuaDaEZtxPM1UedJt64BdSZbxNRF4EYfV9ZbruuWsfiVi8FXgkwlkiziBxT0', '2015-01-23 04:27:46', '2015-01-23 04:29:55', NULL),
	(44, 'test12@test.ru', '$2y$10$JqZYW76sIbN56LDWjXp06e3O39n3od3o8o5KSRHZx2mX6QotCLzQe', NULL, '2015-01-23 04:32:01', '2015-01-23 04:32:01', NULL),
	(45, 'test13@test.ru', '$2y$10$lXeFyHeKZpIktAV4dh1vRevsjNTxQ8MCrVODuuJNfSA7mmjIcgSim', 'RzYKeydGDZevhO0Wg5EOFX0ygjsS7LinhTYTKLERoXOzNDzOENMGKpR9QpuS', '2015-01-23 04:35:03', '2015-01-23 04:55:46', NULL),
	(46, 'bekaa@mirsushi.net', '$2y$10$9XxVrNjsWLmIQHA0A5kGy.rR9Kn/ktFhLJvtXRJf2Z/OiZhN38IWq', 'oYZqeTcrVpQyV1LjmTBj3DBABfvIP6jyALz78eKDvACWgfBk1YbkROL4dVGw', '2015-01-23 05:02:05', '2015-01-23 05:04:25', NULL),
	(47, 'akomarov@greenpeace.org', '$2y$10$XI.pEZJYUDTfqZG7WH7ocuYfeIpB48ePMnDawMmBP16GfBQi6KzUm', '7tdtNk86hrTU0lKWJmXPYX7HrJwwk5eOyyKlhITr0WzIpZvayF1GF8YRCDP6', '2015-01-23 10:46:21', '2015-03-01 16:36:06', NULL),
	(48, 'ilona.zhuravleva@greenpeace.org', '$2y$10$Pl63QFGzylmiLxnndPpqXO9nrIF7DVXDJgl0mbmglHsfo0PqnGEgK', 'jpv6FjfIpHRM35vlOg3mzclB8xPSKvRVUh4jVZhk6sa7VPna0K2RsKcYNF4e', '2015-01-23 11:06:02', '2015-01-23 11:13:56', NULL),
	(50, 'zhilona@yandex.ru', '$2y$10$CVEwFmWvuwLT./nmkAGhNesyvRsnFKcWrZpLjj.4IY45fdeZXyiIy', NULL, '2015-01-28 09:44:01', '2015-01-28 09:44:01', NULL),
	(51, '15@test.ru', '$2y$10$siO26qv9d2cwsIvD369OFuIxqm7Ncc44YOTU/5j6z/ry5rBnoOzgW', NULL, '2015-02-27 08:28:47', '2015-02-27 08:28:47', NULL),
	(52, 'cvbnm@yandex.ru', '$2y$10$fzLY6is/LrbZ0UOydll/dOMaXy.vjtngnQ7J9aQlAixKWv.1DZCD2', 'gfoEv9Xj4riafnLuKR5WG39T2IltUo586TKo1NiXfohqOpHeKvuC5kBPy1W0', '2015-03-01 14:27:03', '2015-03-01 14:30:25', NULL),
	(53, '123@123.ru', '$2y$10$cwb.qO571TwAI7VbdNlSsuTw8lZmvg.cUnFsuP/0166tC.7ZmqKz.', 'ri8Av7AgJ6YZzefIJ6mjfRP9nqHRX170CBRoyJn5jq0xhC8TGRaS0skBDVJg', '2015-03-01 14:30:51', '2015-03-01 14:35:40', NULL),
	(54, 'Designing@future.com', '$2y$10$6hjq6Qx4QxQ.v.XEb55SruH7K8GVHRRb.FtkCkT/qvzJgEWxaJP6q', 'RJzkkWOjEAIvYek1jpDcyEKgtjzUS2wd5pnRXPXkEFyULYhb14YXWwcACSy0', '2015-03-01 14:40:16', '2015-03-01 14:47:33', NULL),
	(55, 'dashazu@kina.org', '$2y$10$nc9x/bKWFWAdDrRXl4Q5s.w6RGYPil/8sLSKiwt1zRkZ3UlqGQVRm', '0rCEKRMQKRXyBCMgyP84keLwYopibuFm4vG6Ep4uEulWkkHCXOVk1xJzUIU4', '2015-03-01 14:48:15', '2015-03-01 14:56:53', NULL),
	(56, 'as@as.ru', '$2y$10$FPz.sKHjVHs0cOqlndrcvOLhVcs45A/8PB2O3Uqo0SDb.aFvp1xD6', 'V4I8dFYdZFHaKAJdmWHS6SMU2Ig8w8VUcbQtsxLpqpibLnK0EnZTeWJpZgI2', '2015-03-01 16:37:34', '2015-03-01 16:40:30', NULL),
	(57, 'mylo@gmylo.com', '$2y$10$PfbnrwbEP5V/LaDJipQTY./EdjZpEpddBFQe8kma7n3sziVa3FvQ2', 'T8z0a2p0Ac9m6mJxvx23R3uQvmvYsrkNn8Z0Skl3ncwWW3BYzZ0zBMTN26wg', '2015-03-01 16:41:04', '2015-03-01 16:45:41', NULL),
	(58, '7@777.ru', '$2y$10$HpBqsSvJpa262AsfMQ0SN.I/MAq6p06rNlaJWOUNeRLLLVF9RMIAa', 'pua1bbJBosbrXxqkFJGNTtNPv13Nf6RuykoLHCAvRcI3xL7sYjOE7i0wu9GM', '2015-03-01 16:49:22', '2015-03-01 16:51:58', NULL),
	(59, 'parol@mylo.ru', '$2y$10$ThvD6RIe7NgYbSQTAqLmR.tyGJh3KL8OQP4x5tzYkwjAifCWD4UIO', NULL, '2015-03-01 16:52:41', '2015-03-01 16:52:41', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
