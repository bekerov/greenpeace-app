/**
 * Created by Bekerov Artur on 18.01.15.
 */
var crmApp = angular.module('crmApp', [
    'ngRoute',
    'ngCookies',
    'crmControllers',
    'crmServices'
]);

crmApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/login', {
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            }).
            when('/register', {
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'
            }).
            when('/profile', {
                templateUrl: 'templates/profile.html',
                controller: 'ProfileCtrl'
            }).
            when('/admin', {
                templateUrl: 'templates/admin/admin.html',
                controller: 'AdminCtrl'
            }).
            when('/admin/forms', {
                templateUrl: 'templates/admin/forms.html',
                controller: 'FormsAdminCtrl'
            }).
            when('/admin/form/:id', {
                templateUrl: 'templates/admin/form.html',
                controller: 'FormAdminCtrl'
            }).
            otherwise({
                redirectTo: '/profile'
            });
    }]);


crmApp.filter('dateToISO', function() {
    return function(input) {
        input = new Date(input).toISOString();
        return input;
    };
});