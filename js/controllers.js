/**
 * Created by Bekerov Artur on 18.01.15.
 */
var crmControllers = angular.module('crmControllers', ['ngResource', 'ui.utils', 'ui.bootstrap', 'angularFileUpload']);

crmControllers.controller('SampleCtrl', ['$scope', '$http',
    function ($scope, $http) {

    }]);

crmControllers.controller('HeaderCtrl', ['$scope', '$http', '$location', 'Logout',
    function ($scope, $http, $location, Logout) {

        $scope.logout = function() {
            Logout.get({},
                function success(data) {
                    $location.path("/login");
                },
                function err(error) {
                });
        }

    }]);

crmControllers.controller('LoginCtrl', ['$scope', '$http', '$location', 'Login', 'ResetPassword',
    function ($scope, $http, $location, Login, ResetPassword ) {

        $scope.hide = true;

        $scope.LogIn = function() {

            $scope.ajaxLogin = true;

            Login.post($scope.sign,
                function(data) {
                    if (data.error == true) {
                        alert('Неверный логин и пароль')
                    } else {
                        $location.path("/profile");
                    }
                    $scope.ajaxLogin = false;
                });
        }

        $scope.forgot = function() {
//            ResetPassword.post($scope.sign, function (res) {
//
//            });
        }
    }]);

crmControllers.controller('RegisterCtrl', ['$scope', '$http', '$location', 'Register',
    function ($scope, $http, $location, Register) {

        $scope.Register = function() {

            $scope.ajaxLogin = true;

            Register.post($scope.sign,
                function success(data) {

                    $scope.ajaxLogin = false;
                    console.log(data)
                    $location.path("/profile");
                },
                function err(error) {
                    console.log(error)
                    $scope.ajaxLogin = false;
                });
        }
    }]);


crmControllers.controller('ProfileCtrl', ['$scope', '$http', '$location', '$timeout', 'Profile', 'Logout', 'FileUploader',
    function ($scope, $http, $location, $timeout, Profile, Logout, FileUploader) {
        $scope.form = Profile.get({},
            function success(data) {
                console.log(data)
            },
            function err(error) {
                console.log(error)
                if (error.status == 401) {
                    $location.path("/login");
                }
            });

        // При установке галки "Указанный мобильный телефон"
        $scope.changeContactPhone = function() {
            if ( $scope.form.contact_phone_var == 1 ) {
                $scope.form.contact_phone = $scope.form.mobile_phone;
            } else {
                $scope.form.contact_phone = '';
            }
        }


        $scope.logout = function() {
            Logout.get({},
                function success(data) {
                    $location.path("/login");
                },
                function err(error) {
                });
        }


        $scope.submitForm = function() {
            // Включается анимация кнопки
            $scope.ajaxSave = true;

            Profile.update( {}, $scope.form,
                function success(data) {
                    console.log(data)
                    $scope.ajaxSave = false;

                    $scope.successfulAjax = true;
                    $timeout(function() { $scope.successfulAjax = false; }, 2000)
                },
                function err(error) {
                    console.log(error)
                    $scope.ajaxSave = false;
                });

        }

        // Uploader

        var uploader = $scope.uploader = new FileUploader({
            url: 'upload.php'
        });

        // FILTERS

        uploader.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });

        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

        // /Uploader


    }]);



crmControllers.controller('AdminCtrl', ['$scope', '$http',
    function ($scope, $http) {

    }]);

crmControllers.controller('FormsAdminCtrl', ['$scope', '$http', 'EntryForms',
    function ($scope, $http, EntryForms) {

        $scope.forms = EntryForms.query();

        $scope.setIsBlocked = function (form, state) {
            EntryForms.update( {id: form.id}, {is_blocked: state},
                function success(res) {
                    if ( res.error == false ){
                        form.is_blocked = res.data.is_blocked;
                    }
                    console.log(res)
                },
                function err(error) {
                    console.log(error)
                });
        }

    }]);

crmControllers.controller('FormAdminCtrl', ['$scope', '$http', '$routeParams', 'EntryForms',
    function ($scope, $http, $routeParams, EntryForms) {

        $scope.form = EntryForms.get({id: $routeParams.id});

    }]);