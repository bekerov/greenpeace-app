/**
 * Created by Bekerov Artur on 18.01.15.
 */
var crmServices = angular.module('crmServices', ['ngResource']);


crmServices.factory('Login', ['$resource',
    function($resource){
        return $resource('api_v3/public/login', {}, {
            post: {method:'POST'}
        });
    }]);
crmServices.factory('Logout', ['$resource',
    function($resource){
        return $resource('api_v3/public/logout', {}, {
            get: {method:'GET'}
        });
    }]);


crmServices.factory('ResetPassword', ['$resource',
    function ($resource) {
        return $resource('api/reset_pass', {}, { // @todo Доделать
            'post': {method: 'POST'}
        });
    }]);


crmServices.factory('Register', ['$resource',
    function ($resource) {
        return $resource('api_v3/public/register', {}, {
            'post': {method: 'POST'}
        });
    }]);

//crmServices.factory('Profile', ['$resource',
//    function ($resource) {
//        return $resource('api_v3/public/profile', {id: '@id'}, {
//            get: {method: 'GET', isArray: false},
//            query: {method: 'GET', isArray: true},
//            save: {method: 'POST'},
//            delete: {method: 'DELETE'},
//            update: {method: 'PUT'}
//        });
//    }]);

crmServices.factory('Profile', ['$resource',
    function ($resource) {
        return $resource('api_v3/public/profile', {}, {
            get: {method: 'GET', isArray: false},
            query: {method: 'GET', isArray: true},
            save: {method: 'POST'},
            delete: {method: 'DELETE'},
            update: {method: 'PUT'}
        });
    }]);

crmServices.factory('EntryForms', ['$resource',
    function ($resource) {
        return $resource('api_v3/public/entry_forms/:id', {}, {
            get: {method: 'GET', isArray: false},
            query: {method: 'GET', isArray: true},
            save: {method: 'POST'},
            delete: {method: 'DELETE'},
            update: {method: 'PUT'}
        });
    }]);